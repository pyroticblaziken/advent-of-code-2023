import re

from operator import itemgetter

from helpers.utils import parse_file
from rich.console import Console
from rich.table import Table
from rich.segment import Segment
from rich.text import Text

console = Console()

table = Table(title='Configurations')
table.add_column('Row Data', style='yellow')
table.add_column('Length', style='blue')
table.add_column('Broken Data', style='cyan')
table.add_column('Pattern')
table.add_column('Possible Count', style='green')

process_table = Table(title='Process')

raw_data = {}

def match_base(value: str, base: str) -> bool:
    if not len(value) == len(base):
        return False

    for i in range(len(value)):
        if base[i] == '?':
            continue

        if not value[i] == base[i]:
            return False 

    return True


def generate_row_pattern(key: str, add_end: bool):
    pattern = '^\\.*'

    for b in raw_data[key]:
        pattern += f'#{{{b}}}\\.+'

    pattern = pattern[:-3]

    if add_end:
        pattern += '\\.*$'

    return re.compile(pattern)


def original_pattern(data: str) -> re.Pattern:
    pattern = ''

    _q = 0
    for i in range(len(data)):
        if data[i] == '?':
            _q += 1
        elif _q > 0:
            pattern += f'[\\.|#]{{{_q}}}'
            _q = 0
        
        if data[i] == '.':
            pattern += '\\.'
        elif data[i] == '#':
            pattern += '#'
    
    if _q > 0:
        pattern += f'[\\.|#]{{{_q}}}'

    return re.compile(f'^{pattern}$')


def parse_data(lines: list[str]):
    for l in lines:
        parts = l.split(' ')
        raw_data[parts[0]] = [int(i) for i in parts[1].split(',')]


def count_valid_values(data: str, original: str, pattern: re.Pattern) -> int:
    if data.count('?') == 0:
        is_valid = not pattern.match(data) == None and match_base(data, original)
        return 1 if is_valid else 0
    
    return count_valid_values(data.replace('?', '.', 1), original, pattern) + count_valid_values(data.replace('?', '#', 1), original, pattern)


def count_valid_values_test(data: str, original: str, pattern: re.Pattern, should_match_base: bool) -> int:
    if data.count('?') == 0:
        is_valid = not pattern.match(data) == None
        if should_match_base:
            is_valid = is_valid and match_base(data, original)
        return 1 if is_valid else 0
    
    return count_valid_values_test(data.replace('?', '.', 1), original, pattern, should_match_base) + count_valid_values_test(data.replace('?', '#', 1), original, pattern, should_match_base)


def count_valid_with_sum(data: str, pattern: re.Pattern, sum: int) -> int:
    if data.count('#') > sum:
            return 0
    
    if data.count('?') == 0:
        return 1 if not pattern.match(data) == None else 0    
        
    return count_valid_with_sum(data.replace('?', '.', 1), pattern, sum) + count_valid_with_sum(data.replace('?', '#', 1), pattern, sum)

def generate_valid_values(data: str, values: list[str], pattern: re.Pattern):
    if data.count('?') == 0:
        is_valid = not pattern.match(data) == None
        if is_valid: values.append(data)
        return

    generate_valid_values(data.replace('?', '.', 1), values, pattern)
    generate_valid_values(data.replace('?', '#', 1), values, pattern)


def generate_all_values(data: str, values: list[str]):
    if data.count('?') == 0:
        values.append(data)
        return

    generate_all_values(data.replace('?', '.', 1), values)
    generate_all_values(data.replace('?', '#', 1), values)


def test_all_data():
    test_table = Table(title=f'Testing Multiple Ways...')
    test_table.add_column('Row')
    test_table.add_column('Broken')
    test_table.add_column('B Sum')
    test_table.add_column('Alpha', style='cyan')
    test_table.add_column('Beta', style='magenta')
    test_table.add_column('Gamma', style='yellow')

    totals = {}
    totals['a'] = 0
    totals['b'] = 0
    totals['c'] = 0

    table_rows = []

    for k in raw_data:
        broken_sum = sum(raw_data[k])
        if not broken_sum == 3:
            continue

        pattern_a = generate_row_pattern(k, True)
        pattern_b = generate_row_pattern(k, False)
        _a = count_valid_values_test(k, k, pattern_a, True)
        _b = count_valid_values_test(k, k, pattern_b, True)
        _c = count_valid_with_sum(k, pattern_b, broken_sum)
        # test_table.add_row(k, f'{raw_data[k]}', f'{len(k)}', f'{_a}', f'{_c}')
        totals['a'] += _a
        totals['b'] += _b
        totals['c'] += _c
        if not _a == _b or not _b == _c: table_rows.append([k, f'{raw_data[k]}', f'{broken_sum}', _a, _b, _c])
    

    for r in table_rows:
        test_table.add_row(r[0], r[1], r[2], f'{r[3]}', f'{r[4]}', f'{r[5]}')

    test_table.add_row('Totals', '-', '-', f'{totals['a']}', f'{totals['b']}', f'{totals['c']}')
    console.log(test_table)


def test_single_key(key: str):
    test_table = Table(title=f'Testing {key}')
    test_table.add_column('Value')
    test_table.add_column('Alpha')
    test_table.add_column('Beta')

    total_broken = sum(raw_data[key])

    console.log(f'Key: {key}')
    console.log(f'Row: {raw_data[key]}')
    console.log(f'Length: {len(key)}')
    console.log(f'Total Broken: {total_broken}')

    pattern_alpha = generate_row_pattern(key, True)
    pattern_beta = generate_row_pattern(key, False)

    console.log(f'Alpha: {pattern_alpha}')
    console.log(f'Beta : {pattern_beta}')

    potential = []
    generate_all_values(key, potential)
    total = 0

    for p in potential:
        if p.count('#') > total_broken:
            continue

        _a = not pattern_alpha.match(p) == None
        _b = not pattern_beta.match(p) == None
        if _a == _b and _a == True:
            test_table.add_row(p, f'{_a}', f'{_b}')
            total += 1
        elif not _a == _b:
            _a_segment = Text(text=f'{_a}', style='cyan')
            _b_segment = Text(text=f'{_b}', style='yellow')
            test_table.add_row(p, _a_segment, _b_segment)
    
    console.print(test_table)
    console.print(total)



# 7614 Answer is too low
# 11646 Answer is too high
# 32352 Answer is too high
if __name__ == '__main__':
    lines = parse_file('src/aoc_2023/data/day_12.txt')
    parse_data(lines)
    total = 0

    test_all_data()

    # key = '??????????'
    # test_single_key(key)
