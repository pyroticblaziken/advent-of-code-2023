import re
from typing import List, Tuple
from helpers.utils import parse_file
from rich.console import Console
from rich.table import Table
from functools import reduce

console = Console()

# 540345 is too high
# 540037 is too low
def points_to_check(x: int, y: int, len: int) -> list[Tuple]:
    points = []

    for i in range(-1, len + 1):
        _x = x + i
        points.append((_x, y - 1))
        points.append((_x, y + 1))

        if _x < x or _x > x + len - 1:
            points.append((_x, y))
        
    return points


trouble = [6, 19, 28, 32, 35, 67, 100, 103, 104, 114, 117]

def get_part_totals(lines: List[str]) -> int:
    total = 0
    other = 0
    number = re.compile('\\d+')
    y = 0

    table = Table(title="Part Numbers")
    table.add_column("Part Number")
    table.add_column("Line", justify="center")
    table.add_column("Pos", justify="center")
    table.add_column("Only Periods", justify="center")
    table.add_column("Periods & Digits", justify="center")
    table.add_column("Surrounding Characters")

    for l in lines:
        potentials = number.findall(l)
        if y in trouble:
            print(f'Line {y}: {potentials}')
        for p in potentials:
            only_periods = False
            did_add = False
            surrounding_characters = ''
            pos = l.index(p)
            points = points_to_check(pos, y, len(p))
            
            for c in points:
                if c[0] < 0 or c[0] >= len(l) or c[1] < 0 or c[1] >= len(lines):
                    continue

                surrounding_characters += lines[c[1]][c[0]]

                if not lines[c[1]][c[0]] == '.' and not only_periods:
                    other += int(p)
                    only_periods = True

                if not lines[c[1]][c[0]] == '.' and number.match(lines[c[1]][c[0]]) == None and not did_add:
                    total += int(p)
                    did_add = True

            if (not only_periods == did_add): 
                table.add_row(f'{p}', f'{y}', f'{pos}', f'{only_periods}', f'{did_add}', surrounding_characters)

        y += 1

    # console.print(table)
    print(f'The only periods total is: {other}')
    return total


def get_part_total_ex(lines: List[str]) -> int:
    total = 0
    number = re.compile('\\d+')
    x = 0
    y = 0

    for l in lines:
        while True:
            result = number.search(l, x)

            if result == None:
                break

            _indicies = result.span()
            pos = _indicies[0]
            length = _indicies[1] - pos

            points = points_to_check(pos, y, length)
            for p in points:
                if p[0] < 0 or p[0] >= len(l) or p[1] < 0 or p[1] >= len(lines):
                    continue

                if not lines[p[1]][p[0]] == '.' and number.match(lines[p[1]][p[0]]) == None:
                    total += int(l[pos:pos+length])
                    break

            x = pos + length
        
        x = 0
        y += 1

    return total


def find_gear_ratios(lines: list[str]) -> int:
    total = 0
    number = re.compile('\\d+')
    x = 0
    y = 0
    adjacent_parts = {}

    for l in lines:
        while True:
            i = l.find('*', x)
            if i == -1:
                break

            adjacent_parts[(i, y)] = []
            x = i + 1
        
        x = 0
        y += 1
    
    x = 0
    y = 0

    

    for l in lines:
        while True:
            result = number.search(l, x)

            if result == None:
                break

            _indicies = result.span()
            pos = _indicies[0]
            length = _indicies[1] - pos

            points = points_to_check(pos, y, length)
            for p in points:
                if p[0] < 0 or p[0] >= len(l) or p[1] < 0 or p[1] >= len(lines):
                    continue
            
                if not lines[p[1]][p[0]] == '.':
                    if lines[p[1]][p[0]] == '*' and p in adjacent_parts:
                        adjacent_parts[p].append(int(l[pos:pos+length]))
                    break

            x = pos + length
        
        x = 0
        y += 1

    print(adjacent_parts)

    for k in adjacent_parts:
        if len(adjacent_parts[k]) == 2:
            total += reduce(lambda x, y: x * y, adjacent_parts[k])

    return total


if __name__ == '__main__':
    lines = parse_file('src/aoc_2023/data/day_03.txt')
    # total = get_part_total_ex(lines)
    total = find_gear_ratios(lines)
    print(f'Part Number totals: {total}')
