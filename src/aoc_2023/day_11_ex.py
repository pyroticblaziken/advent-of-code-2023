import re
from copy import deepcopy
from helpers.algorithms import a_star
from helpers.utils import manhattan_distance, parse_file
from rich.console import Console
from rich.table import Table
from functools import reduce
from itertools import repeat

console = Console()

original = []
galaxies = []
double_rows = []
double_cols = []


def parse_data(lines: list[str], distance: int = 2):
    for y in range(len(lines)):
        if all([c == '.' for c in lines[y]]):
            double_rows.append(y)
        
        for x in range(len(lines[y])):
            if y == 0 and all ([c == '.' for c in [l[x] for l in lines]]):
                double_cols.append(x)
            
            if lines[y][x] == '#':
                original.append((x, y))
                _x = x + sum([distance - 1 for c in double_cols if c < x])
                _y = y + sum([distance - 1 for r in double_rows if r < y])
                galaxies.append((_x, _y))


def sum_of_distances():
    total = 0

    for i in range(len(galaxies) - 1):
        for j in range(i, len(galaxies)):
            total += manhattan_distance(galaxies[i], galaxies[j])

    return total


if __name__ == '__main__':
    lines = parse_file('src/aoc_2023/data/day_11.txt')
    parse_data(lines, 1000000)
    table = Table(title="Galaxies")
    table.add_column('Original', style='cyan')
    table.add_column('Adjusted', style='green')

    for i in range(len(original)):
        table.add_row(f'{original[i]}', f'{galaxies[i]}')
    
    console.print(table)
    console.print(f'Total of all distances: {sum_of_distances()}')