import re

from operator import itemgetter

from helpers.utils import parse_file, is_power_of_two
from rich.console import Console
from rich.table import Table
from rich.segment import Segment
from rich.text import Text

console = Console()

raw_data = []
areas = []
flipped_areas = []


def area_str_to_number(value: str) -> int:
    b_string = value.replace('#', '1').replace('.', '0')

    return int(b_string, base=2)


def flip_area(area: list[str]) -> list[str]:
    new_area = []

    for i in range(len(area[0])):
        new_area.append(''.join([a[i] for a in area]))
    
    return new_area


def parse_data(lines: list[str]):
    _data = []

    for l in lines:
        if l == '':
            raw_data.append(_data)
            _data = []
        else:
            _data.append(l)
    
    if len(_data) > 0:
        raw_data.append(_data)
    
    for r in raw_data:
        areas.append([area_str_to_number(_r) for _r in r])
        flipped_areas.append([area_str_to_number(_r) for _r in flip_area(r)])


def find_reflection_point(area: list[int]) -> int:
    for reflect in range(1, len(area)):
        _l = reflect - 1
        _u = reflect
        could_be = True
        while _l > -1 and _u < len(area):
            could_be = could_be and area[_l] == area[_u]
            _l -= 1
            _u += 1
        
        if could_be:
            return reflect

    return 0


def find_smudge_reflection(area: list[int], debug: bool = False) -> int:
    for reflect in range(1, len(area)):
        _l = reflect - 1
        _u = reflect
        could_be = True
        one_off = False
        while _l > -1 and _u < len(area):
            diff = area[_l] ^ area[_u]
            _l -= 1
            _u += 1

            if diff == 0:
                continue

            if not is_power_of_two(diff):
                could_be = False
                break

            if one_off:
                could_be = False
                break

            if not one_off and is_power_of_two(diff):
                one_off = True
                if debug:
                    console.log(area[_l + 1])
                    console.log(area[_u - 1])
        
        if could_be and one_off:
            return reflect

    return 0


def find_reflections():
    total = 0
    
    for i in range(len(areas)):
        _to_add = find_reflection_point(areas[i]) * 100

        if _to_add == 0:
            _to_add = find_reflection_point(flipped_areas[i])

        total += _to_add

    console.log(f'Total is: {total}')


def find_smudge_reflections():
    total = 0
    
    for i in range(len(areas)):
        _to_add = find_smudge_reflection(areas[i]) * 100

        if _to_add == 0:
            _to_add = find_smudge_reflection(flipped_areas[i])

        total += _to_add

    console.log(f'Total is: {total}')


def print_area_debug(area_id: int, flip_index: int, flip_area: bool = False):
    console.log(f'Area {area_id}:')
    for j in range(len(raw_data[area_id])):
        if j < flip_index:
            console.log(f'[green]{raw_data[area_id][j]}[/green]')
        else:
            console.log(f'[red]{raw_data[area_id][j]}[/red]')
    console.log('')


bugged = [10, 26]

def debug_find_smug_reflections():
    total = 0

    i = 0
    for i in range(len(areas)):
        _to_add = find_smudge_reflection(areas[i], i in bugged)

        if _to_add > 0:
            print_area_debug(i, _to_add)
    
    console.log('Mission Complete!')
    

# 30295 too high
# 16311 too low
if __name__ == '__main__':
    lines = parse_file('src/aoc_2023/data/day_13.txt')
    parse_data(lines)

    find_smudge_reflections()
    # debug_find_smug_reflections()
