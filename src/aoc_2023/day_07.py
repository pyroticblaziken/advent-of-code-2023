import re
from helpers.utils import parse_file
from rich.console import Console
from functools import reduce

console = Console()

camel_hands: list['CamelHand'] = []
# 252886739 is too high

def rank_value(rank: str) -> int:
    match rank:
        case 'A':
            return 14
        case 'K':
            return 13
        case 'Q':
            return 12
        case 'J':
            return 1
        case 'T':
            return 10
        case _:
            return int(rank)
        

def compare_hands(a: str, b: str) -> int:
    score_a = hand_score(a)
    score_b = hand_score(b)

    if not score_a == score_b:
        return score_a - score_b
    
    for i in range(len(a)):
        rank_a = a[i]
        rank_b = b[i]
    
        if not rank_a == rank_b:
            return rank_value(rank_a) - rank_value(rank_b)
    
    return 0


class CamelHand:
    def __init__(self, cards, bid):
        self.cards = cards
        self.bid = bid
    
    def __eq__(self, other: 'CamelHand') -> bool:
        return self.cards == other.cards
    
    def __lt__(self, other: 'CamelHand') -> bool:
        score_a = hand_score_jokers(self.cards)
        score_b = hand_score_jokers(other.cards)

        if not score_a == score_b:
            return score_a < score_b

        for i in range(len(self.cards)):
            rank_a = self.cards[i]
            rank_b = other.cards[i]

            if not rank_a == rank_b:
                return rank_value(rank_a) < rank_value(rank_b)

        return False
    
    def __repr__(self):
        return f'Hand: {self.cards} Bid: {self.bid}'


def hand_score(hand: str) -> int:
    if hand.count(hand[0]) == 5:
        return 7 # Five of a Kind
    
    if hand.count(hand[0]) == 4 or hand.count(hand[1]) == 4:
        return 6 # Four of a Kind 

    ranks = set()
    for c in hand:
        ranks.update(c)

    if len(ranks) == 5:
        return 1 # High Card
    
    if len(ranks) == 4:
        return 2 # One Pair
    
    if len(ranks) == 2:
        return 5 # Full House
    
    if any([hand.count(r) == 3 for r in ranks]):
        return 4 # Must be a three of a kind
    
    return 3 # Is two pairs


def hand_score_jokers(hand: str) -> int:
    if hand.count('J') == 0:
        return hand_score(hand)
    
    if hand.count('J') == 5 or hand.count('J') == 4:
        return 7 # Five of a kind
    
    ranks = set()
    ranks.update([c for c in hand if not c == 'J'])

    if len(ranks) == 1:
        return 7 # Turns all jokers into other rank, so Five of a kind
    
    if len(ranks) == 4:
        return 2 # Turns four different ranks into a single pair

    if hand.count('J') == 3:
        return 6 # Turns two cards into Four of a kind
    
    if hand.count('J') == 2:
        if len(ranks) == 2:
            return 6 # Turns a pair into four of a kind
        else:
            return 4 # Turns three cards into Three of a kind
    
    # There is only one joker left
    _same = max([hand.count(r) for r in ranks])

    if _same == 3:
        return 6 # Three of the same rank means J creates Four of a kind
    elif _same == 2:
        if len(ranks) == 2:
            return 5 # Full House
        else:
            return 4 # Three of a kind
    
    return 2 # Only made one pair


def parse_data(lines: list[str]):
    for l in lines:
        parts = l.split(' ')
        camel_hands.append(CamelHand(parts[0], int(parts[1])))


if __name__ == '__main__':
    lines = parse_file('src/aoc_2023/data/day_07.txt')
    parse_data(lines)
    camel_hands.sort()
    camel_hands = camel_hands[::-1]
    size = len(camel_hands)
    total = 0
    for i in range(0, size):
        winnings = (size - i) * camel_hands[i].bid
        total += winnings

    console.print(f'Total Winnings: {total}')
