import re
from typing import List, Tuple
from helpers.utils import parse_file

def determine_power(game: str) -> int:
    red = 0
    green = 0
    blue = 0
    parts = game.split(':')
    pulls = parts[1].split(';')

    for p in pulls:
        p = p.strip()
        cubes = p.split(',')
        for c in cubes:
            values = c.strip().split(' ') # 0 -> number, 1 is color

            if values[1] == 'red' and int(values[0]) > red:
                red = int(values[0])
            
            if values[1] == 'green' and int(values[0]) > green:
                green = int(values[0])
            
            if values[1] == 'blue' and int(values[0]) > blue:
                blue = int(values[0])

    return red * green * blue


def determine_game_possible(game: str, red:int = 12, green:int = 13, blue:int = 14) -> int:
    parts = game.split(':')
    id = int(parts[0].split(' ')[1])
    pulls = parts[1].split(';')

    for p in pulls:
        p = p.strip()
        cubes = p.split(',')
        for c in cubes:
            values = c.strip().split(' ') # 0 -> number, 1 is color

            if values[1] == 'red' and int(values[0]) > red:
                return 0
            
            if values[1] == 'green' and int(values[0]) > green:
                return 0
            
            if values[1] == 'blue' and int(values[0]) > blue:
                return 0

    return id


def determine_possible_games(games: List[str]) -> int:
    total = 0

    for game in games:
        total += determine_game_possible(game)

    return total


def determine_total_power(games: List[str]) -> int:
    total = 0

    for game in games:
        total += determine_power(game)

    return total


if __name__ == '__main__':
    lines = parse_file('src/aoc_2023/data/day_02.txt')
    total = determine_total_power(lines)
    print(f'The total power is: {total}')
