import re
from copy import deepcopy
from helpers.algorithms import a_star
from helpers.utils import manhattan_distance, parse_file
from rich.console import Console
from functools import reduce
from itertools import repeat

console = Console()

galaxies = []
double_rows = []
double_cols = []
all_nodes = []
large_cost = 2


def parse_data(lines: list[str]):
    for y in range(len(lines)):
        if all([c == '.' for c in lines[y]]):
            double_rows.append(y)

        for x in range(len(lines[y])):
            if y == 0 and all([c == '.' for c in [l[x] for l in lines]]):
                double_cols.append(x)
            
            all_nodes.append((x, y))
            
            if lines[y][x] == '#':
                galaxies.append((x, y))
            

def is_between(x, a, b) -> bool:
    return (x < a and x > b) or (x > a and x < b)


def next_nodes(_, node: tuple) -> list[tuple]:
    pot = [(node[0] - 1, node[1]), (node[0] + 1, node[1]), (node[0], node[1] - 1), (node[0], node[1] + 1)]
    return [p for p in pot if p in all_nodes]


def node_cost(_, node: tuple) -> int:
    return 1


def calculate_distance(a, b) -> int:
    path = a_star(a, b, all_nodes, next_nodes, manhattan_distance, node_cost)
    distance = 0

    for p in path[1:]:
        if p[0] in double_cols or p[1] in double_rows:
            distance += large_cost
        else:
            distance += 1

    console.log(f'The distance between {a} and {b} is {distance}')

    return distance


def sum_of_distances() -> int:
    total = 0

    for i in range(len(galaxies) - 1):
        for j in range(i + 1, len(galaxies)):
            dist = calculate_distance(galaxies[i], galaxies[j])
            total += dist
    
    return total


if __name__ == '__main__':
    lines = parse_file('src/aoc_2023/data/test_11.txt')
    parse_data(lines)

    console.log(f'The total distance between all galaxy pairs is: {sum_of_distances()}')
