import re
from copy import deepcopy
from helpers.utils import parse_file
from rich.console import Console
from functools import reduce

console = Console()

grid = {}
loop = []


def traverse_pipe(pos: tuple, dir: int) -> list | None:
    next_pos = (pos[0], pos[1] - 1) # North
    match dir:
        case 1:
            next_pos = (pos[0] + 1, pos[1]) # East
        case 2:
            next_pos = (pos[0], pos[1] + 1) # South
        case 3:
            next_pos = (pos[0] - 1, pos[1]) # West
    
    if not next_pos in grid:
        return None
    
    if grid[next_pos] == '.':
        return None
    
    can_traverse = False
    if dir == 0 and grid[next_pos] in ['|', 'F', '7']:
        can_traverse = True
    elif dir == 1 and grid[next_pos] in ['-', '7', 'J']:
        can_traverse = True
    elif dir == 2 and grid[next_pos] in ['|', 'J', 'L']:
        can_traverse = True
    elif dir == 3 and grid[next_pos] in ['-', 'F', 'L']:
        can_traverse = True
    
    if can_traverse:
        match grid[next_pos]:
            case '|':
                return [next_pos, dir]
            case '-':
                return [next_pos, dir]
            case 'F':
                return [next_pos, 1 if dir == 0 else 2]
            case '7':
                return [next_pos, 3 if dir == 0 else 2]
            case 'L':
                return [next_pos, 1 if dir == 2 else 0]
            case 'J':
                return [next_pos, 3 if dir == 2 else 0]
            
    return None


def generate_grid(lines: list[str]):
    start = None
    for y in range(len(lines)):
        for x in range(len(lines[y])):
            grid[(x, y)] = lines[y][x]
            if lines[y][x] == 'S':
                start = (x, y)
    
    return start


def count_steps(start: tuple):
    steps = 0
    _start = start
    _start_dir = 0 # 1
    _end = start
    _end_dir = 3 # 2
    loop.append(start)
    should_finish = False

    while not should_finish:
        _n_start = traverse_pipe(_start, _start_dir)
        _n_end = traverse_pipe(_end, _end_dir)
        steps += 1

        if _n_start == None or _n_end == None:
            console.log(f'Completed {steps} steps before failure...')
            console.log(f'Start Result: {_n_start}')
            console.log(f'End Result: {_n_end}')
            return None
        
        _start, _start_dir = _n_start
        _end, _end_dir = _n_end

        should_finish = _start == _end
        loop.append(_end)
        if not should_finish:
            loop.insert(0, _start)


    return steps

def determine_interior():
    h = max([k[1] for k in grid])
    w = max([k[0] for k in grid])

    console.log(f'The grid is ({w},{h})')
    count = 0

    vert = False
    hori = False

    for x in range(w):
        for y in range(h):
            pos = (x, y)
            if pos in loop:
                if not grid[pos] == '-':
                    hori = not hori
                if not grid[pos] == '|':
                    vert = not vert
            else:
                if vert and hori:
                    count += 1

    return count


def render_grid():
    h = max([k[1] for k in grid])
    w = max([k[0] for k in grid])

    for y in range(h):
        to_render = ''
        for x in range(w):
            pos = (x, y)
            if pos in loop:
                to_render += f'[magenta]{grid[pos]}[/magenta]'
            else:
                to_render += f'[white]{grid[pos]}[/white]'

        console.log(to_render)


# 1624 is too high
if __name__ == '__main__':
    lines = parse_file('src/aoc_2023/data/day_10.txt')
    # console.log(lines)
    start = generate_grid(lines)
    console.log(start)
    steps = count_steps(start)
    console.log(f'It took {steps} steps to complete the loop.')
    console.log(f'The loop is {len(loop)} long')
    render_grid()
    # interior = determine_interior()
    # console.log(f'The interior spaces are {interior}')
