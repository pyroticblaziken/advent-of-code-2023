import re

from copy import deepcopy
from operator import itemgetter

from helpers.utils import parse_file, is_power_of_two
from rich.console import Console
from rich.segment import Segment
from rich.status import Status
from rich.table import Table
from rich.text import Text

console = Console()

cube_rocks = []
round_rocks = []
limits = [0, 0]


def tilt_north():
    global round_rocks
    new_rocks = deepcopy(round_rocks)

    for i in range(len(round_rocks)):
        r = round_rocks[i]
        p_cubes = [c[1] for c in cube_rocks if c[0] == r[0] and c[1] < r[1]]
        c_max = max(p_cubes) if len(p_cubes) > 0 else -1
        p_round = sum([1 for q in round_rocks if q[0] == r[0] and q[1] < r[1] and q[1] > c_max])
        new_rocks[i] = (r[0], c_max + p_round + 1)

    round_rocks.clear()
    round_rocks += new_rocks


def tilt_south():
    global round_rocks
    new_rocks = deepcopy(round_rocks)

    for i in range(len(round_rocks)):
        r = round_rocks[i]
        p_cubes = [c[1] for c in cube_rocks if c[0] == r[0] and c[1] > r[1]]
        c_min = min(p_cubes) if len(p_cubes) > 0 else limits[1]
        p_round = sum([1 for q in round_rocks if q[0] == r[0] and q[1] > r[1] and q[1] < c_min])
        new_rocks[i] = (r[0], c_min - p_round - 1)
    
    round_rocks.clear()
    round_rocks += new_rocks


def tilt_west():
    global round_rocks
    new_rocks = deepcopy(round_rocks)

    for i in range(len(round_rocks)):
        r = round_rocks[i]
        p_cubes = [c[0] for c in cube_rocks if c[1] == r[1] and c[0] < r[0]]
        c_max = max(p_cubes) if len(p_cubes) > 0 else -1
        p_round = sum([1 for q in round_rocks if q[1] == r[1] and q[0] < r[0] and q[0] > c_max])
        new_rocks[i] = (c_max + p_round + 1, r[1])
    
    round_rocks.clear()
    round_rocks += new_rocks


def tilt_east():
    global round_rocks
    new_rocks = deepcopy(round_rocks)

    for i in range(len(round_rocks)):
        r = round_rocks[i]
        p_cubes = [c[0] for c in cube_rocks if c[1] == r[1] and c[0] > r[0]]
        c_min = min(p_cubes) if len(p_cubes) > 0 else limits[0]
        p_round = sum([1 for q in round_rocks if q[1] == r[1] and q[0] > r[0] and q[0] < c_min])
        new_rocks[i] = (c_min - p_round - 1, r[1])
    
    round_rocks.clear()
    round_rocks += new_rocks


def parse_data(lines: list[str]):
    limits[0] = len(lines[0])
    limits[1] = len(lines)

    for y in range(len(lines)):
        for x in range(len(lines[y])):
            if lines[y][x] == 'O':
                round_rocks.append((x, y))
            if lines[y][x] == '#':
                cube_rocks.append((x, y))


def print_debug():
    for y in range(limits[1]):
        _to_write = ''
        for x in range(limits[0]):
            if (x, y) in cube_rocks:
                _to_write += '[yellow]#[/yellow]'
            elif (x, y) in round_rocks:
                _to_write += '[green]O[/green]'
            else:
                _to_write += '[white].[/white]'
        console.log(_to_write)
    
    console.log('')


def calculate_load() -> int:
    return sum([(limits[1] - r[1]) for r in round_rocks])


weight_post_cycle: list[int] = []
whole_maps: list[str] = []

def cycle():
    tilt_north()
    tilt_west()
    tilt_south()
    tilt_east()
    weight_post_cycle.append(calculate_load())
    whole_maps.append(''.join(f'{round_rocks}'))


def find_weight_after_cycles(number_of_cycles: int = 1000000000) -> int:
    loop_found = False
    status = Status(f'Looking for a loop...', console=console, spinner='pong')
    loop = []
    final_cycle = number_of_cycles

    with console.status(status):
        while not loop_found:
            status.update(f'Looking for a loop {len(whole_maps):10}...')
            cycle()
            
            checked = []
            for w in whole_maps:
                if w in checked:
                    continue

                if w not in checked and whole_maps.count(w) >= 3:
                    _to_check = [i for i in range(len(whole_maps)) if whole_maps[i] == w]
                    for i in range(1, len(_to_check)):
                        dist = _to_check[i] - _to_check[0]
                        if dist > len(whole_maps) / 2:
                            break
                        
                        sub_list_a = whole_maps[_to_check[0]:_to_check[i]]
                        sub_list_b = whole_maps[_to_check[i]:_to_check[i] + dist]
                        sub_list_c = whole_maps[_to_check[i] + dist: _to_check[i] + (2 * dist)]

                        if len(sub_list_a) == len(sub_list_b) and len(sub_list_b) == len(sub_list_c):
                            do_match = True
                            for j in range(len(sub_list_a)):
                                do_match = do_match and sub_list_a[j] == sub_list_b[j] and sub_list_b[j] == sub_list_c[j]

                            if do_match:
                                loop_found = True
                                final_cycle -= _to_check[0]
                                loop = sub_list_a
                                break

                checked.append(w)
    
    
    loop_index = final_cycle % len(loop)
    final_weight = whole_maps.index(loop[loop_index])

    return final_weight


if __name__ == '__main__':
    lines = parse_file('src/aoc_2023/data/day_14.txt')
    parse_data(lines)

    weight_post_cycle.append(calculate_load())
    whole_maps.append(''.join(f'{round_rocks}'))
    
    weight = find_weight_after_cycles()

    console.log(f'Last few weights: {weight_post_cycle[:-10]}')
    console.log(f'Final Weight: {weight}')