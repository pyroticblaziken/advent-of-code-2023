import re
from typing import List, Tuple
from helpers.utils import parse_file
from rich.console import Console
from rich.table import Table
from functools import reduce

console = Console()


def card_points(my_nums: list[int], win_nums: list[int]) -> tuple:
    matches = 0

    for m in my_nums:
        if m in win_nums:
            matches += 1

    if matches > 0:
        return (matches, 2 ** (matches - 1))
    
    return (0, 0)


# def total_points(cards: list[str]) -> int:
#     total = 0

#     for c in cards:
#         parts = re.split(r':|\|', c)
#         card_id = parts[0].split(' ')[1]
#         winners = [int(w) for w in [p for p in parts[1].strip().split(' ')] if not w == '']
#         mine = [int(m) for m in [p for p in parts[2].strip().split(' ')] if not m == '']
#         points = card_points(mine, winners)
#         console.print(f'Card {card_id}: {points}')
#         total += points

#     return total


def total_card_count(cards: list[str]) -> int:
    total = 0
    card_score = {}
    card_copies = {}

    for c in cards:
        parts = re.split(r':|\|', c)
        card_id = int(parts[0].split(' ')[-1])
        if not card_id in card_copies:
            card_copies[card_id] = 1
        else:
            card_copies[card_id] += 1
        winners = [int(w) for w in [p for p in parts[1].strip().split(' ')] if not w == '']
        mine = [int(m) for m in [p for p in parts[2].strip().split(' ')] if not m == '']
        output = card_points(mine, winners)
        card_score[card_id] = output[1]
        if output[0] > 0:
            for i in range(card_id + 1, card_id + output[0] + 1):
                if not i in card_copies:
                    card_copies[i] = 1 * card_copies[card_id]
                else:
                    card_copies[i] += 1 * card_copies[card_id]
                
    for k in card_copies:
        if k in card_score:
            total += card_copies[k]

    return total


if __name__ == '__main__':
    lines = parse_file('src/aoc_2023/data/day_04.txt')
    console.print(f'We got a total of {total_card_count(lines)} points')
