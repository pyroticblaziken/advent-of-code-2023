import re
from helpers.utils import parse_file
from rich.console import Console
from functools import reduce

console = Console()

instructions = ''
nodes = {}


def generate_nodes(data: list[str]):
    inst = ''
    pattern = re.compile('(?P<id>[0-9A-Z]+) = \\((?P<left>[0-9A-Z]+), (?P<right>[0-9A-Z]+)\\)')
    for line in data:
        if inst == '':
            inst = line
            continue
        
        m = pattern.match(line)
        if not m == None:
            nodes[m.group('id')] = (m.group('left'), m.group('right'))
        
    return inst


def walk_tree(inst: str, curr: str = 'AAA', is_ghost: bool = True) -> int:
    steps = 0
    index = 0
    should_exit = False

    while not should_exit:
        if inst[index] == 'L':
            curr = nodes[curr][0]
        else:
            curr = nodes[curr][1]
        
        steps += 1
        index += 1

        if index >= len(inst):
            index = 0

        if is_ghost:
            should_exit = curr == 'ZZZ'
        else:
            should_exit = curr[2] == 'Z'
    
    return steps


def ghost_walk(inst: str) -> dict:
    ghost_paths = {}
    curr = [k for k in nodes if k[2] == 'A']

    for c in curr:
        ghost_paths[c] = walk_tree(inst, c, False)
    
    return ghost_paths
    

if __name__ == '__main__':
    lines = parse_file('src/aoc_2023/data/day_08.txt')
    instructions = generate_nodes(lines)

    # console.log(f'Instructions: {instructions}')
    # console.log(nodes)
    # steps = walk_tree(instructions)
    # console.print(f'It took {steps} to walk the desert')

    ghost_paths = ghost_walk(instructions)
    console.print(f'Ghost Paths: {ghost_paths}')
    # Calculate Least Common Multiple (i.e. put it into the internet)
    # 12324145107121
