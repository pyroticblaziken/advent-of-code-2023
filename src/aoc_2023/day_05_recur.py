import re
import sys
from typing import List, Tuple
from operator import itemgetter
from functools import reduce
from multiprocessing import Manager, Process
from time import sleep

from helpers.rich import Header, make_default_layout, make_job_progress, make_progress_table
from helpers.utils import parse_file

from rich.console import Console
from rich.table import Table
from rich.status import Status
from rich.progress import Progress, SpinnerColumn, BarColumn, TextColumn
from rich.live import Live
from rich.panel import Panel

console = Console()

layout = make_default_layout()
layout['header'].update(Header('Seed Location', 2021, 5))

seed_to_soil = []
soil_to_fertilizer = []
fertilizer_to_water = []
water_to_light = []
light_to_temp = []
temp_to_humid = []
humid_to_loc = []
seed_ranges = []
possible_lowests = {}

def prepare_translations(data: list[str]) -> list[int]:
    numbers = re.compile('\\d+')
    step = 0
    seeds = []

    for line in data:
        if line == '':
            continue

        parts = line.split(' ')

        if not numbers.match(line) == None:
            if step == 1:
                seed_to_soil.append([int(p) for p in parts])
            elif step == 2:
                soil_to_fertilizer.append([int(p) for p in parts])
            elif step == 3:
                fertilizer_to_water.append([int(p) for p in parts])
            elif step == 4:
                water_to_light.append([int(p) for p in parts])
            elif step == 5:
                light_to_temp.append([int(p) for p in parts])
            elif step == 6:
                temp_to_humid.append([int(p) for p in parts])
            else:
                humid_to_loc.append([int(p) for p in parts])
            continue

        match parts[0]:
            case 'seeds:':
                seeds = [int(p) for p in parts[1:]]
            case 'seed-to-soil':
                step = 1
            case 'soil-to-fertilizer':
                step = 2
            case 'fertilizer-to-water':
                step = 3
            case 'water-to-light':
                step = 4
            case 'light-to-temperature':
                step = 5
            case 'temperature-to-humidity':
                step = 6
            case 'humidity-to-location':
                step = 7
            case _:
                console.print(parts[0])

    return seeds


def translate_location(l: int, translations: list[list]) -> int:
    for t in translations:
        if l >= t[1] and l < t[1] + t[2]:
            return l - t[1] + t[0]

    return l


def forward_translate(l: int) -> int:
    l = translate_location(l, seed_to_soil)
    l = translate_location(l, soil_to_fertilizer)
    l = translate_location(l, fertilizer_to_water)
    l = translate_location(l, water_to_light)
    l = translate_location(l, light_to_temp)
    l = translate_location(l, temp_to_humid)
    l = translate_location(l, humid_to_loc)

    return l


def process_seed_range(seeds, process_limit: int = 100) -> int:
    # console.log(f'Processing: {seeds}')
    if len(seeds) <= process_limit:
        lowest = sys.maxsize

        for s in seeds:
            to_test = forward_translate(s)
            if to_test < lowest:
                lowest = to_test
        
        return lowest

    split = int(len(seeds) / 2)
    left_range = seeds[:split]
    right_range = range(left_range.stop, seeds.stop)
    left = process_seed_range(left_range)
    right = process_seed_range(right_range)

    if left < right:
        return left
    
    return right
    

if __name__ == '__main__':
    lines = parse_file('src/aoc_2023/data/day_05.txt')
    seeds = prepare_translations(lines)

    # Sort the lists for display
    seed_to_soil = sorted(seed_to_soil, key=itemgetter(1))
    soil_to_fertilizer = sorted(soil_to_fertilizer, key=itemgetter(1))
    fertilizer_to_water = sorted(fertilizer_to_water, key=itemgetter(1))
    water_to_light = sorted(water_to_light, key=itemgetter(1))
    light_to_temp = sorted(light_to_temp, key=itemgetter(1))
    temp_to_humid = sorted(temp_to_humid, key=itemgetter(1))
    humid_to_loc = sorted(humid_to_loc, key=itemgetter(1))

    # Make Seed Ranges
    index = 0
    while index < len(seeds):
        seed_ranges.append((seeds[index], seeds[index + 1]))
        index += 2

    seed_ranges = sorted(seed_ranges, key=itemgetter(0))
    seed_lists = []

    console.log('Creating ranges...')
    for s_r in seed_ranges:
        seed_lists.append(range(s_r[0], s_r[0] + s_r[1]))
    console.log('Finished creating ranges.')

    lowest = sys.maxsize
    status = Status('Processing seeds...', console=console, spinner='pong')
    with console.status(status):
        for s_r in seed_lists:
            status.update(f'Currently processing {s_r}')
            lowest = process_seed_range(s_r)
    
    console.print(f'Found lowest: {lowest}')
