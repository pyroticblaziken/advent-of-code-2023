import re
from typing import List
from helpers.utils import parse_file


def input_to_digit(input: str) -> str:
    if len(input) == 1:
        return input
    
    match input:
        case 'one':
            return '1'
        case 'two':
            return '2'
        case 'three':
            return '3'
        case 'four':
            return '4'
        case 'five':
            return '5'
        case 'six':
            return '6'
        case 'seven':
            return '7'
        case 'eight':
            return '8'
        case 'nine':
            return '9'
        case _:
            return '0'


def get_calibration_value(lines: List[str]) -> int:
    digits = re.compile('\\d|one|two|three|four|five|six|seven|eight|nine')
    sum = 0

    for l in lines:
        matches = digits.findall(l)
        num = input_to_digit(matches[0]) + input_to_digit(matches[-1])
        sum += int(num)
    
    return sum


if __name__ == '__main__':
    lines = parse_file('src/aoc_2023/data/day_01.txt')
    sum = get_calibration_value(lines)
    print(f'The sum is: {sum}')
