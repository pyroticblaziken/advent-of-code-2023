import re
from helpers.utils import parse_file
from rich.console import Console
from functools import reduce

console = Console()


def create_race_results(data: list[str]) -> list[tuple]:
    time = [d for d in data[0].split(' ') if not d == '']
    distance = [d for d in data[1].split(' ') if not d == '']

    output = []
    for i in range(1, len(time)):
        output.append((int(time[i]), int(distance[i])))
    
    return output

def convert_race_results(data: list[str]) -> tuple:
    _t = [d for d in data[0].split(' ') if not d == '']
    _d = [d for d in data[1].split(' ') if not d == '']

    time_str = ''.join(_t[1:])
    dist_str = ''.join(_d[1:])

    return (int(time_str), int(dist_str))


def determine_winning_strategies(time: int, distance: int) -> int:
    low = 1
    high = time - 1

    low_win = False
    high_win = False

    while not low_win:
        if low * (time - low) > distance:
            low_win = True
            break

        low += 1
    
    while not high_win:
        if high * (time - high) > distance:
            high_win = True
            break

        high -= 1

    return len(range(low, high + 1))

if __name__ == '__main__':
    lines = parse_file('src/aoc_2023/data/day_06.txt')
    race_results = create_race_results(lines)
    strategies = [determine_winning_strategies(r[0], r[1]) for r in race_results]
    console.print(reduce(lambda x, y: x * y, strategies))
    single_race = convert_race_results(lines)
    long_win = determine_winning_strategies(single_race[0], single_race[1])
    console.print(long_win)
