import re
import sys
from typing import List, Tuple
from operator import itemgetter
from functools import reduce
from multiprocessing import Manager, Process
from time import sleep

from helpers.rich import Header, make_default_layout, make_job_progress, make_progress_table
from helpers.utils import parse_file

from rich.console import Console
from rich.table import Table
from rich.status import Status
from rich.progress import Progress, SpinnerColumn, BarColumn, TextColumn
from rich.live import Live
from rich.panel import Panel

from tqdm.contrib.concurrent import process_map

console = Console()

layout = make_default_layout()
layout['header'].update(Header('Seed Location', 2021, 5))

seed_to_soil = []
soil_to_fertilizer = []
fertilizer_to_water = []
water_to_light = []
light_to_temp = []
temp_to_humid = []
humid_to_loc = []
seed_ranges = []
possible_lowests = {}


def prepare_translations(data: list[str]) -> list[int]:
    numbers = re.compile('\\d+')
    step = 0
    seeds = []

    for line in data:
        if line == '':
            continue

        parts = line.split(' ')

        if not numbers.match(line) == None:
            if step == 1:
                seed_to_soil.append([int(p) for p in parts])
            elif step == 2:
                soil_to_fertilizer.append([int(p) for p in parts])
            elif step == 3:
                fertilizer_to_water.append([int(p) for p in parts])
            elif step == 4:
                water_to_light.append([int(p) for p in parts])
            elif step == 5:
                light_to_temp.append([int(p) for p in parts])
            elif step == 6:
                temp_to_humid.append([int(p) for p in parts])
            else:
                humid_to_loc.append([int(p) for p in parts])
            continue

        match parts[0]:
            case 'seeds:':
                seeds = [int(p) for p in parts[1:]]
            case 'seed-to-soil':
                step = 1
            case 'soil-to-fertilizer':
                step = 2
            case 'fertilizer-to-water':
                step = 3
            case 'water-to-light':
                step = 4
            case 'light-to-temperature':
                step = 5
            case 'temperature-to-humidity':
                step = 6
            case 'humidity-to-location':
                step = 7
            case _:
                console.print(parts[0])

    return seeds


def translate_location(l: int, translations: list[list]) -> int:
    for t in translations:
        if l >= t[1] and l < t[1] + t[2]:
            return l - t[1] + t[0]

    return l 


def reverse_translate(l: int, translations: list[list]) -> int:
    for t in translations:
        if l >= t[0] and l < t[0] + t[2]:
            return l + t[1] - t[0]
    return l


def forward_translate(l: int) -> int:
    l = translate_location(l, seed_to_soil)
    l = translate_location(l, soil_to_fertilizer)
    l = translate_location(l, fertilizer_to_water)
    l = translate_location(l, water_to_light)
    l = translate_location(l, light_to_temp)
    l = translate_location(l, temp_to_humid)
    l = translate_location(l, humid_to_loc)

    return l


def forward_translate_process(start: int, size: int, job_progress: Progress, task_id: int, return_dict: dict):
    lowest = sys.maxsize

    for i in range(0, size):
        to_test = translate_location(i + start, seed_to_soil)
        to_test = translate_location(to_test, soil_to_fertilizer)
        to_test = translate_location(to_test, fertilizer_to_water)
        to_test = translate_location(to_test, water_to_light)
        to_test = translate_location(to_test, light_to_temp)
        to_test = translate_location(to_test, temp_to_humid)
        to_test = translate_location(to_test, humid_to_loc)

        if to_test < lowest:
            lowest = to_test
            return_dict[task_id] = lowest
        
        job_progress.advance(task_id, float(i)/size)


def forward_translate_process_helper(args: list) -> int:
    return forward_translate_process(args[0], args[1], args[2], args[3], args[4])


def reverse_translate_process(start: int, size: int, job_progress: Progress, task_id: int):
    lowest = sys.maxsize

    for i in range(0, size):
        to_test = reverse_translate(i + start, humid_to_loc)
        to_test = reverse_translate(to_test, temp_to_humid)
        to_test = reverse_translate(to_test, light_to_temp)
        to_test = reverse_translate(to_test, water_to_light)
        to_test = reverse_translate(to_test, fertilizer_to_water)
        to_test = reverse_translate(to_test, soil_to_fertilizer)
        to_test = reverse_translate(to_test, seed_to_soil)

        if to_test < lowest:
            lowest = to_test
        
        job_progress.advance(task_id, float(i)/size)

    possible_lowests[task_id] = lowest
    console.print(lowest)


def reverse_translate_process_helper(args: list):
    reverse_translate_process(args[0], args[1], args[2], args[3])


if __name__ == '__main__':
    lines = parse_file('src/aoc_2023/data/day_05.txt')
    seeds = prepare_translations(lines)

    # Sort the lists for display
    seed_to_soil = sorted(seed_to_soil, key=itemgetter(1))
    soil_to_fertilizer = sorted(soil_to_fertilizer, key=itemgetter(1))
    fertilizer_to_water = sorted(fertilizer_to_water, key=itemgetter(1))
    water_to_light = sorted(water_to_light, key=itemgetter(1))
    light_to_temp = sorted(light_to_temp, key=itemgetter(1))
    temp_to_humid = sorted(temp_to_humid, key=itemgetter(1))
    humid_to_loc = sorted(humid_to_loc, key=itemgetter(1))

    lowest = sys.maxsize

    job_progress = make_job_progress()
    dest_ranges = [r[1:] for r in humid_to_loc]

    # Make Seed Ranges
    index = 0
    while index < len(seeds):
        seed_ranges.append((seeds[index], seeds[index + 1]))
        index += 2

    seed_ranges = sorted(seed_ranges, key=itemgetter(0))
    parameter_sets = []
    manager = Manager()
    return_dict = manager.dict()
    processes = []

    # for sr in seed_ranges:
    #     _task_id = job_progress.add_task(f'Seed Range: ({sr[0]},{sr[1]})')
    #     _p = Process(target=forward_translate_process, args=(sr[0], sr[1], job_progress, _task_id, return_dict))
    #     processes.append(_p)
    #     _p.start()

    # # for proc in processes:
    # #     proc.join()

    # layout['data_vis'].update(make_progress_table(job_progress))

    # with Live(layout, screen=True):
    #     while any([p.is_alive() for p in processes]):
    #         layout['data_vis'].update(make_progress_table(job_progress))

    #         process_table = Table('Process Id', 'Is Alive')
    #         for p in processes:
    #             process_table.add_row(f'{p._identity}', f'{p.is_alive()}')
    #         layout['log'].update(Panel(process_table, style='Yellow'))

    #         return_table = Table('Task_Id', 'Lowest')
    #         for k in return_dict:
    #             return_table.add_row(f'{k}', f'{return_dict[k]}')
    #         layout['variables'].update(Panel(return_table, style='Red'))
    
    # console.print(layout)
    # console.print(return_dict)

    status = Status(f'Processing...', console=console, spinner='pong')

    with console.status(job_progress):
        for s_r in seed_ranges:
            _task_id = job_progress.add_task(f'Seed Range: ({s_r[0]},{s_r[1]})')
            for i in range(100):
                _value = forward_translate(i + s_r[0])
                if _value < lowest:
                    lowest = _value
                
                progress = i / s_r[1]
                job_progress.advance(_task_id, 1/s_r[1])

    console.print(job_progress)
    console.print(lowest)
