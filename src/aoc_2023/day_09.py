import re
from helpers.utils import parse_file
from rich.console import Console
from functools import reduce

console = Console()


def generate_derivation(data: list[int]) -> list[int]:
    return [(data[i + 1] - data[i]) for i in range(len(data) - 1)]


def calculate_next(history: list[int]) -> int:
    if all([v == 0 for v in histories]):
        return 0
    
    if all([v == history[0] for v in history]):
        return history[0]

    return history[-1] + calculate_next(generate_derivation(history))


def calculate_previous(history: list[int]) -> int:
    if all([v == 0 for v in histories]):
        return 0
    
    if all([v == history[0] for v in history]):
        return history[0]
    
    return history[0] - calculate_previous(generate_derivation(history))


if __name__ == '__main__':
    lines = parse_file('src/aoc_2023/data/day_09.txt')
    histories = [[int(p) for p in l.split(' ')] for l in lines]
    # console.log(histories)
    # console.log(generate_derivation(histories[0]))
    sum = 0
    for h in histories:
        sum += calculate_previous(h)
    
    console.log(sum)
