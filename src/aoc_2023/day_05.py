import re
import sys
from typing import List, Tuple
from helpers.utils import parse_file
from rich.console import Console
from rich.table import Table
from rich.status import Status
from functools import reduce
from operator import itemgetter
from tqdm.contrib.concurrent import process_map

console = Console()

seed_to_soil = []
soil_to_fertilizer = []
fertilizer_to_water = []
water_to_light = []
light_to_temp = []
temp_to_humid = []
humid_to_loc = []


def reverse_tranlation(l: int, translations: list[list]) -> int:
    for t in translations:
        if l >= t[0] and l < t[0] + t[2]:
            return l + t[1] - t[0]
    return l


def full_reverse(start, size) -> int:
    lowest = sys.maxsize

    for i in range(start, start + size):
        to_test = reverse_tranlation(i, humid_to_loc)
        to_test = reverse_tranlation(to_test, temp_to_humid)
        to_test = reverse_tranlation(to_test, light_to_temp)
        to_test = reverse_tranlation(to_test, water_to_light)
        to_test = reverse_tranlation(to_test, fertilizer_to_water)
        to_test = reverse_tranlation(to_test, soil_to_fertilizer)
        to_test = reverse_tranlation(to_test, seed_to_soil)

        if i < lowest:
            lowest = i
    
    return lowest


def translate_location(l: int, translations: list[list]) -> int:
    for t in translations:
        if l >= t[1] and l < t[1] + t[2]:
            return l - t[1] + t[0]

    return l 


def prepare_translations(data: list[str]) -> list[int]:
    numbers = re.compile('\\d+')
    step = 0
    seeds = []

    for line in data:
        if line == '':
            continue

        parts = line.split(' ')

        if not numbers.match(line) == None:
            if step == 1:
                seed_to_soil.append([int(p) for p in parts])
            elif step == 2:
                soil_to_fertilizer.append([int(p) for p in parts])
            elif step == 3:
                fertilizer_to_water.append([int(p) for p in parts])
            elif step == 4:
                water_to_light.append([int(p) for p in parts])
            elif step == 5:
                light_to_temp.append([int(p) for p in parts])
            elif step == 6:
                temp_to_humid.append([int(p) for p in parts])
            else:
                humid_to_loc.append([int(p) for p in parts])
            continue

        match parts[0]:
            case 'seeds:':
                seeds = [int(p) for p in parts[1:]]
            case 'seed-to-soil':
                step = 1
            case 'soil-to-fertilizer':
                step = 2
            case 'fertilizer-to-water':
                step = 3
            case 'water-to-light':
                step = 4
            case 'light-to-temperature':
                step = 5
            case 'temperature-to-humidity':
                step = 6
            case 'humidity-to-location':
                step = 7
            case _:
                console.print(parts[0])

    return seeds


if __name__ == '__main__':
    lines = parse_file('src/aoc_2023/data/day_05.txt')
    seeds = prepare_translations(lines)
    lowest = sys.maxsize

    seed_to_soil = sorted(seed_to_soil, key=itemgetter(1))
    soil_to_fertilizer = sorted(soil_to_fertilizer, key=itemgetter(1))
    fertilizer_to_water = sorted(fertilizer_to_water, key=itemgetter(1))
    water_to_light = sorted(water_to_light, key=itemgetter(1))
    light_to_temp = sorted(light_to_temp, key=itemgetter(1))
    temp_to_humid = sorted(temp_to_humid, key=itemgetter(1))
    humid_to_loc = sorted(humid_to_loc, key=itemgetter(1))

    # Forward Translation given a small number of seeds
    # for s in seeds:
    #     s = translate_location(s, seed_to_soil)
    #     s = translate_location(s, soil_to_fertilizer)
    #     s = translate_location(s, fertilizer_to_water)
    #     s = translate_location(s, water_to_light)
    #     s = translate_location(s, light_to_temp)
    #     s = translate_location(s, temp_to_humid)
    #     s = translate_location(s, humid_to_loc)

    #     if s < lowest:
    #         lowest = s

    # Reverse Translation
    location = -1
    status = Status(f'Calculating lowest location {location}', console=console, spinner='pong')
    with console.status(status):
        for t in humid_to_loc:
            for l in range(t[1], t[1] + t[2]):
                status.update(f'Testing {l}...')
                to_test = t[1]
                to_test = reverse_tranlation(to_test, humid_to_loc)
                to_test = reverse_tranlation(to_test, temp_to_humid)
                to_test = reverse_tranlation(to_test, light_to_temp)
                to_test = reverse_tranlation(to_test, water_to_light)
                to_test = reverse_tranlation(to_test, fertilizer_to_water)
                to_test = reverse_tranlation(to_test, soil_to_fertilizer)
                to_test = reverse_tranlation(to_test, seed_to_soil)
            
            if to_test < lowest:
                lowest = to_test
    
    console.print(f'The nearest location is: {lowest}')
