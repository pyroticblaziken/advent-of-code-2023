import re

from copy import deepcopy
from operator import itemgetter

from helpers.utils import parse_file, is_power_of_two
from rich.console import Console
from rich.segment import Segment
from rich.status import Status
from rich.table import Table
from rich.text import Text

console = Console()

data_chunks: list[str] = []
boxes = {}

class Lens:
    def __init__(self, label: str, focal_length: int):
        self.label = label
        self.focal_length = focal_length

    def __str__(self: 'Lens'):
        return f'Lens [{self.label}]: {self.focal_length}'
    
    def __repr__(self):
        return self.__str__()


def hash_data(value: str) -> int:
    output = 0

    for c in value:
        output += ord(c)
        output *= 17
        output %= 256

    return output


def initialization_sequence():
    for chunk in data_chunks:
        split_index = chunk.index('-') if '-' in chunk else chunk.index('=')
        label = chunk[:split_index]
        b_index = hash_data(label)

        # console.log(f'Processing {chunk} => Label: {label}')

        if '-' in chunk: # Remove lens if it is there
            l_index = [i for i in range(len(boxes[b_index])) if boxes[b_index][i].label == label]
            if len(l_index) > 0:
                boxes[b_index] = boxes[b_index][:l_index[0]] + boxes[b_index][l_index[0]+1:]
        elif '=' in chunk:
            f_value = int(chunk[chunk.index('=')+1:])
            l_index = [i for i in range(len(boxes[b_index])) if boxes[b_index][i].label == label]
            if len(l_index) > 0:
                boxes[b_index][l_index[0]].focal_length = f_value
            else:
                boxes[b_index].append(Lens(label, f_value))


def calculate_focus_power() -> int:
    focus_power = 0

    for k in boxes:
        if len(boxes[k]) > 0:
            _fp = sum([(k + 1) * (i + 1) * boxes[k][i].focal_length for i in range(len(boxes[k]))])
            # console.log(f'Focus Power for Box[{k}]: {_fp}')
            focus_power += _fp

    return focus_power


def parse_data(lines: list[str]):
    for i in range(256):
        boxes[i] = []

    global data_chunks
    data_chunks += lines[0].split(',')


def hash_all_chunks() -> int:
    total = 0

    for chunk in data_chunks:
        _v = hash_data(chunk)
        # console.log(f'Hash of {chunk}: {_v}')
        total += _v
    
    return total


if __name__ == '__main__':
    lines = parse_file('src/aoc_2023/data/day_15.txt')
    parse_data(lines)

    initialization_sequence()

    # for b in boxes:
    #     if len(boxes[b]) > 0:
    #         console.log(f'Box {b:03}: {boxes[b]}')

    focus_power = calculate_focus_power()

    console.log(f'Total Focus Power: {focus_power}')
